#!/usr/bin/env python
# coding: utf-8

# # Data Science Survey:EDA
# #By- Shubham Kumar
# #Dated: July 30,2021

# In[1]:


from IPython.display import Image
Image(url='https://espresso-prod.s3.amazonaws.com/files/styles/adaptive_mid/s3/images/20170125_dap309.jpg?itok=FDbK6tOT')


# In[2]:


import numpy as np 
import pandas as pd 
import altair as alt
alt.data_transformers.disable_max_rows()
alt.themes.enable('fivethirtyeight')


# ## Responses per Country

# In[3]:


df_survey_responses = pd.read_csv("/home/aarush100616/Downloads/Projects/Cloud War/kaggle_survey_2020_responses.csv")
df_multi_choice = pd.read_csv("/home/aarush100616/Downloads/Projects/Cloud War/multiple_choice_responses.csv")


# In[4]:


df_survey_responses['Q3'].unique()


# In[5]:


def abbreviate_nations(df):
    df = df.drop(0, axis=0)
    df.loc[df['Q3']=="United Kingdom of Great Britain and Northern Ireland", "Q3"] = "United Kingdom"
    df.loc[df['Q3']=="Iran, Islamic Republic of...", "Q3"] = "Iran"
    df.loc[df['Q3']=="Republic of Korea", "Q3"] = "South Korea"
    return df


# In[6]:


col_list = df_survey_responses.columns.to_list()
col_list
#create a mapping dict from question id to question content
question_mapping = df_survey_responses.loc[0].to_dict()
print(question_mapping)
df_survey_responses = abbreviate_nations(df_survey_responses)
df_multi_choice = abbreviate_nations(df_multi_choice)


# In[7]:


country_2019 = df_multi_choice.groupby(['Q3'])
df_country_2019 = country_2019.agg('count')
df_country_2019.reset_index(inplace=True)
df_country_2019=df_country_2019[['Q3', 'Q1']]
df_country_2019.columns=('Country', 'Count')
df_country_2019['year'] = 2019
df_country_2019.tail()


# In[8]:


country = df_survey_responses.groupby(['Q3'])
df_country = country.agg('count')
df_country.reset_index(inplace=True)
df_country=df_country[['Q3', 'Q1']]
df_country.columns=('Country', 'Count')
df_country['year'] = 2020
df_country.tail()


# In[9]:


df_country_comparison = df_country.append(df_country_2019).reset_index(drop=True)
df_country_comparison.tail()


# In[10]:


def chart_country(df, sort=None):
    chart_country_groups = alt.Chart(df_country).mark_bar().encode(
    alt.X('Country:N', sort=sort, axis=alt.Axis(grid=False)),
    alt.Y('Count:Q', title="Count of Responses", axis=alt.Axis(grid=False))
    ).properties(width=680,
    background='#f5f5f5',title={
"text": ["Number of Respondents per Country"],
"fontSize":20, "fontWeight":600, "subtitleFontSize":14
}
    )
    
    return chart_country_groups
chart_country_groups = chart_country(df_country)


# In[11]:


df_country = df_country.sort_values(by=['Count'], ascending=False)
country_sort = df_country['Country'].to_list()
chart_country_groups = chart_country(df_country)
chart_country_groups


# In[12]:


df_country_count_by_year = df_country.merge(df_country_2019, left_on='Country', right_on='Country',
          suffixes=(None, '_2019'))
df_country_count_by_year.tail()


# In[13]:


import matplotlib
import matplotlib.pyplot as plt
x = np.arange(len(df_country_count_by_year['Country']))  # the label locations
width = 0.35  # the width of the bars
fig, ax = plt.subplots(figsize=(15, 6))
rects1 = ax.bar(x - width/2, df_country_count_by_year['Count_2019'], width, label='2019')
rects2 = ax.bar(x + width/2, df_country_count_by_year['Count'], width, label='2020')
 # Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('Responses', fontsize=14)
ax.set_xlabel('Country', fontsize=14)
ax.set_title('Survey Responses by Country and Year', fontsize=18)
ax.set_xticks(x)
ax.set_xticklabels(df_country_count_by_year['Country'])
ax.set_facecolor('k')
ax.patch.set_alpha(0.25)
plt.xticks(rotation=90)
fig.patch.set_alpha(0.5)
ax.legend()
ax.annotate('Responses from India increased', xy=(0.5, 4250), xytext=(3, 4500),
            arrowprops=dict(facecolor='black'),
            )
ax.annotate('Responses from the USA decreased', xy=(1.5, 2000), xytext=(4, 2250),
            arrowprops=dict(facecolor='black'),
            )
#fig.tight_layout()
plt.show()


# In[14]:


df_country_count_by_year['yoy_change_pct'] = 100 * (df_country_count_by_year['Count'] - df_country_count_by_year['Count_2019']) / df_country_count_by_year['Count_2019']


# In[15]:


alt.Chart(df_country_count_by_year).mark_bar().encode(
    alt.X('Country:N', axis=alt.Axis(grid=False, labelAngle=270)),
    alt.Y('yoy_change_pct:Q', title="Year-on-Year Change in Responses %", axis=alt.Axis(grid=False)),
    color=alt.condition(
        alt.datum.yoy_change_pct > 0,
        alt.value("steelblue"),  # The positive color
        alt.value("orange")  # The negative color
    )
).properties(width=680,
    background='#f5f5f5',title={
"text": ["% Year-on-Year Change in Responses by Country from 2019 to 2020"],
"fontSize":20, "fontWeight":600, "subtitleFontSize":14
}
    )


# In[16]:


import pycountry
import plotly.express as px
countries, counts = np.unique(df_country_count_by_year['Country'].values.tolist(), return_counts=True)
countries_w_codes = {}
for country in pycountry.countries:
    countries_w_codes[country.name] = country.alpha_3

codes = [countries_w_codes.get(country, f'{country}') for country in countries]


# In[17]:


corrected_codes = {'Iran': 'IRN', 'Republic of Korea': 'PRK',
                   'Russia': 'RUS', 'South Korea': 'KOR', 'Taiwan': 'TWN',
                   'United Kingdom of Great Britain and Northern Ireland': 'GBR',
                   'United States of America': 'USA'}

codes_crr = []
for code in codes:
    if code in corrected_codes.keys():
        codes_crr.append(corrected_codes[code])
    else:
        codes_crr.append(code)
        
country_codes ={}
for country, code in zip(countries, codes_crr):
    country_codes[country] = code


# In[18]:


country_alterations ={'Nepal': 'NPL', 'Sri Lanka':'LKA', 'United Arab Emirates':'ARE', 'Ghana':'GHA'}
country_codes.update(country_alterations)
country_codes


# In[19]:


df_country_count_by_year['country_code'] = df_country_count_by_year['Country']
df_country_count_by_year.replace({"country_code": country_codes}, inplace=True)
df_country_count_by_year['yoy_change_pct'] = np.round(df_country_count_by_year['yoy_change_pct'],1)
df_country_count_by_year


# In[20]:


fig = px.choropleth(data_frame=df_country_count_by_year,
                    locations='country_code',
                    color='yoy_change_pct',
                    color_continuous_scale="turbo",
                    range_color=(-40, 100),
                    hover_name='Country',
                    title='% Year on year change in Survey Responses')
fig.show()


# ## Breakdown by Country and Age

# In[21]:


age_country = df_survey_responses.groupby(['Q1', 'Q3'])
df_age_country = age_country.agg('count')
df_age_country.reset_index(inplace=True)
df_age_country=df_age_country[['Q1', 'Q3', 'Q2']]
df_age_country.columns=('Age', 'Country', 'Count')
df_age_country['LogCount'] = np.log(df_age_country['Count'])
#sort by count
df_age_country = df_age_country.sort_values(by=['Count'], ascending=False)
df_age_country.head()


# In[22]:


alt.Chart(df_age_country).mark_rect().encode(
    alt.X('Country:N', sort=None, title="Country sorted by descending count of aggregate responses"),
    alt.Y('Age:N'),
    color=alt.Color('LogCount:Q', legend=alt.Legend(title="Log Count of Respondents", orient="top"))
).properties(width=680,
    background='#f5f5f5',title={
"text": ["Age of Respondents"],
"subtitle": ["Broken Down by Country"],
"fontSize":20, "fontWeight":600, "subtitleFontSize":14
}
)


# In[23]:


df_age_country = df_age_country.merge(df_country, left_on='Country', right_on='Country',
          suffixes=(None, '_total'))
df_age_country


# In[24]:


df_age_country['age_group_pct'] = 100 * df_age_country['Count'] / df_age_country['Count_total']
df_age_country


# In[25]:


alt.Chart(df_age_country).mark_rect().encode(
    alt.X('Country:N', sort=None, title="Country sorted by descending count of aggregate responses"),
    alt.Y('Age:N'),
    color=alt.Color('age_group_pct:Q', legend=alt.Legend(title="% of Respondents", orient="top"))
).properties(width=680,
    background='#f5f5f5',title={
"text": ["Age of Respondents"],
"subtitle": ["Broken Down proportionally by Country"],
"fontSize":20, "fontWeight":600, "subtitleFontSize":14
}
)


# In[26]:


df_age_country_under_25 = df_age_country.loc[df_age_country['Age'].isin(['18-21', '22-24'])]

under_25 = df_age_country_under_25.groupby(['Country'])
df_under_25 = under_25.agg('sum')
df_under_25.reset_index(inplace=True)
df_under_25=df_under_25[['Country', 'age_group_pct']]
df_under_25.columns=('Country', 'pct_under_25')
df_under_25['pct_under_25'] = round(df_under_25['pct_under_25'],1)
df_under_25['country_code'] = df_under_25['Country']
df_under_25.replace({"country_code": country_codes}, inplace=True)
df_under_25


# In[27]:


fig = px.choropleth(data_frame=df_under_25,
                    locations='country_code',
                    color='pct_under_25',
                    color_continuous_scale="agsunset",
                    range_color=(0, 75),
                    hover_name='Country', # column to add to hover information
                    title='% Respondents under 25 years of Age')
fig.show()


# In[28]:


#only top countries

alt.Chart(df_age_country[df_age_country['Country'].isin(country_sort[:9])]).mark_bar(
    cornerRadiusTopLeft=3,
    cornerRadiusTopRight=3
).encode(
    x=alt.X('Count:Q', stack="normalize", title="Normalized Count"),
    y=alt.Y('Age:O'),
    color=alt.Color('Country:N', legend=alt.Legend(orient="right"))
).properties(width=320,
             background='#f5f5f5',title={
"text": ["Age of Respondents"],
"subtitle": ["Broken Down proportionally by Country"],
"fontSize":20, "fontWeight":600, "subtitleFontSize":14
})


# In[29]:


education_country = df_survey_responses.groupby(['Q4', 'Q3'])
df_education_country = education_country.agg('count')
df_education_country.reset_index(inplace=True)
df_education_country=df_education_country[['Q4', 'Q3', 'Q2']]
df_education_country.columns=('education', 'country', 'Count')
df_education_country['rank'] = df_education_country['education']
education_sort_map = {"Doctoral degree": 6, 'Master’s degree': 5, "Professional degree": 4, 'Bachelor’s degree': 3, 
                  "Some college/university study without earning a bachelor’s degree": 2,
                  "No formal education past high school": 1, "I prefer not to answer": 0
                 }
df_education_country['rank'] = df_education_country['rank'].replace(education_sort_map)
df_education_country['education_rank'] = df_education_country['rank'].astype(str) + ' - ' + df_education_country['education']


# In[30]:


alt.Chart(df_education_country).mark_bar(
    cornerRadiusTopLeft=3,
    cornerRadiusTopRight=3
).encode(
    x=alt.X('country:N'),
    y=alt.Y('Count:Q', stack="normalize", title="Normalized Count"),
    color=alt.Color('education_rank:N', legend=alt.Legend(orient="left"))
).properties(width=520,
             background='#f5f5f5',title={
"text": ["Highest Educational Achievement Held by Respondents"],
"subtitle": ["Broken Down proportionally by Country"],
"fontSize":18, "fontWeight":600, "subtitleFontSize":14
})


# In[31]:


platform_options ={'Q37_Part_1': 'Coursera', 'Q37_Part_2': 'edX', 'Q37_Part_3': 'Kaggle Learn Courses',
                   'Q37_Part_4': 'DataCamp', 'Q37_Part_5': 'Fast.ai', 'Q37_Part_6': 'Udacity',
                   'Q37_Part_7': 'Udemy', 'Q37_Part_8': 'LinkedIn Learning', 'Q37_Part_9': 'Cloud-certification programs (direct from AWS, Azure, GCP, or similar)',
                   'Q37_Part_10': 'University Courses (resulting in a university degree)', 'Q37_Part_11': 'None', 'Q37_OTHER': 'Other',
                  }

for key in platform_options:
    ed_platform = df_survey_responses.groupby([key, 'Q3'])
    df_ed_platform = ed_platform.agg('count')
    df_ed_platform.reset_index(inplace=True)
    df_ed_platform = df_ed_platform[[key, 'Q3', 'Q2']]
    df_ed_platform.columns = ('Platform', 'country', 'Count')
    if key=='Q37_Part_1':
        df_ed_platform_full = df_ed_platform
    else:
        df_ed_platform_full = df_ed_platform_full.append(df_ed_platform)
#shorten longest country names
df_ed_platform_full.loc[df_ed_platform_full['Platform']=="Cloud-certification programs (direct from AWS, Azure, GCP, or similar)", "Platform"] = "Cloud-certification programs"
df_ed_platform_full.loc[df_ed_platform_full['Platform']=="University Courses (resulting in a university degree)", "Platform"] = "University Courses"
df_ed_platform_full['logCount'] = np.log(df_ed_platform_full['Count'])
df_ed_platform_full


# In[32]:


def platform_altair(df, colorfeat, colortitle):
    chrt = alt.Chart(df_ed_platform_full).mark_rect().encode(
        x=alt.X('country:N'),
        y=alt.Y('Platform:N'),
        color=alt.Color(colorfeat, legend=alt.Legend(orient="bottom"), title=colortitle)
    ).properties(width=600, height=200,
                 background='#f5f5f5',title={
    "text": ["Online Platforms Used by Respondents"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    })
    return chrt


# In[33]:


chrt = platform_altair(df_ed_platform_full, 'Count:Q', 'Count')
chrt


# In[34]:


log_chrt = platform_altair(df_ed_platform_full, 'logCount:Q', 'Log Count')
log_chrt


# In[35]:


df_ed_platform_full = df_ed_platform_full.merge(df_country, left_on='country', right_on='Country',
          suffixes=(None, '_total'))
df_ed_platform_full['platform_pct'] = round(100 * df_ed_platform_full['Count'] / df_ed_platform_full['Count_total'],1)
df_ed_platform_full


# In[36]:


pct_chrt = platform_altair(df_ed_platform_full, 'platform_pct:Q', '% in Country')
pct_chrt


# In[37]:


df_ed_platform_full.loc[df_ed_platform_full['Country']=='Belarus']


# In[38]:


job_country = df_survey_responses.groupby(['Q5', 'Q3'])
df_job_country = job_country.agg('count')
df_job_country.reset_index(inplace=True)
df_job_country=df_job_country[['Q5', 'Q3', 'Q2']]
df_job_country.columns=('job', 'country', 'Count')


# In[39]:


alt.Chart(df_job_country).mark_bar(
    cornerRadiusTopLeft=3,
    cornerRadiusTopRight=3
).encode(
    x=alt.X('country:N'),
    y=alt.Y('Count:Q', stack="normalize", title="Normalized Count"),
    color=alt.Color('job:N', legend=alt.Legend(orient="left"))
).properties(width=550,
             background='#f5f5f5',title={
"text": ["Job Titles Held by Respondents"],
"subtitle": ["Broken Down proportionally by Country"],
"fontSize":18, "fontWeight":600, "subtitleFontSize":14
})


# In[40]:


df_job_proportions = df_job_country.merge(df_country, left_on='country', right_on='Country',
          suffixes=(None, '_total'))
df_job_proportions.drop(columns=['country'], inplace=True)
df_job_proportions['job_proportion'] = df_job_proportions['Count'] / df_job_proportions['Count_total']
df_job_proportions


# In[41]:


df_job_proportions_data_scientists = df_job_proportions.loc[df_job_proportions['job']=="Data Scientist"]
df_job_proportions_data_scientists


# In[42]:


alt.Chart(df_job_proportions_data_scientists).mark_bar(color="grey").encode(
    alt.Y('Country:N', axis=alt.Axis(grid=False)),
    alt.X('job_proportion:Q', title="Proportion of Respondents", axis=alt.Axis(grid=False))
    ).properties(width=480, height=500,
    background='#f5f5f5',title={
"text": ["Proportion of Respondents who are Data Scientists"],
"subtitle": ["Broken Down by Country"],
"fontSize":20, "fontWeight":600, "subtitleFontSize":14
})


# ## Breakdown by Years of Coding Experience

# In[43]:


experience_country = df_survey_responses.groupby(['Q6', 'Q3'])
df_experience_country = experience_country.agg('count')
df_experience_country.reset_index(inplace=True)
df_experience_country=df_experience_country[['Q6', 'Q3', 'Q2']]
df_experience_country.columns=('experience', 'country', 'Count')

df_experience_country['rank'] = df_experience_country['experience']
experience_sort_map = {"20+ years": 6, '10-20 years': 5, "5-10 years": 4, '3-5 years': 3, 
                  "1-2 years": 2,
                  "< 1 years": 1, "I have never written code": 0
                 }
df_experience_country['rank'] = df_experience_country['rank'].replace(experience_sort_map)
df_experience_country['experience_rank'] = df_experience_country['rank'].astype(str) + ' - ' + df_experience_country['experience']
df_experience_country


# In[44]:


alt.Chart(df_experience_country).mark_bar(
    cornerRadiusTopLeft=3,
    cornerRadiusTopRight=3
).encode(
    y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
    x=alt.X('Count:Q', stack="normalize", title="Normalized Count"),
    color=alt.Color('experience_rank:N', legend=alt.Legend(orient="bottom"), title="Years of Experience")
).properties(width=480, height=520,
             background='#f5f5f5',title={
"text": ["Years of Experience"],
"subtitle": ["Broken Down proportionally by Country"],
"fontSize":18, "fontWeight":600, "subtitleFontSize":14
})


# ## What Language Would you Recommend an aspiring Data Scientist Learn First?

# In[45]:


prog_lang_country = df_survey_responses.groupby(['Q8', 'Q3'])
df_prog_lang_country = prog_lang_country.agg('count')
df_prog_lang_country.reset_index(inplace=True)
df_prog_lang_country=df_prog_lang_country[['Q8', 'Q3', 'Q2']]
df_prog_lang_country.columns=('prog_lang', 'country', 'Count')
#sort by count
df_prog_lang_country = df_prog_lang_country.sort_values(by=['Count'], ascending=False)
df_prog_lang_country.head()


# In[46]:


def prog_lang_country(df, title):
    selection = alt.selection_multi(fields=['prog_lang'], bind='legend')
    
    chart_prog_lang_country = alt.Chart(df).mark_bar(
    ).encode(
        y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
        x=alt.X('Count:Q', stack="normalize", title="Normalized Count"),
        color=alt.Color('prog_lang:N', legend=alt.Legend(orient="bottom"), title="Programming Language"),
        opacity=alt.condition(selection, alt.value(1), alt.value(0.2))
    ).properties(width=480, height=500,
                 background='#f5f5f5',title={
    "text": [title],
    "subtitle": ["Broken Down proportionally by Country"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    }).add_selection(
    selection
    )
    
    return chart_prog_lang_country


# In[47]:


prog_lang_country(df_prog_lang_country, "Recommended Starting Programming Language for Data Science")


# ## What type of computing platform do you use most often for your data science projects?

# In[48]:


platform_country = df_survey_responses.groupby(['Q11', 'Q3'])
df_platform_country = platform_country.agg('count')
df_platform_country.reset_index(inplace=True)
df_platform_country=df_platform_country[['Q11', 'Q3', 'Q2']]
df_platform_country.columns=('platform', 'country', 'Count')
#sort by count
df_platform_country = df_platform_country.sort_values(by=['Count'], ascending=False)
df_platform_country.head()


# In[49]:


def platform_country_chart(df, title):
    chart_platform_country = alt.Chart(df).mark_bar(
        cornerRadiusTopLeft=3,
        cornerRadiusTopRight=3
    ).encode(
        y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
        x=alt.X('Count:Q', stack="normalize", title="Normalized Count"),
        color=alt.Color('platform:N', legend=alt.Legend(orient="top"), title="Platform")
    ).properties(width=400, height=500,
                 background='#f5f5f5',title={
    "text": [title],
    "subtitle": ["Broken Down proportionally by Country"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    })
    return chart_platform_country


# In[50]:


alt.themes.enable('vox')
platform_country_chart(df_platform_country, "Preferred Platform for Data Science")


# In[51]:


df_platform_country = df_platform_country.loc[df_platform_country['platform']!="A personal computer or laptop"]
alt.themes.enable('vox')
platform_country_chart(df_platform_country, "Preferred Platform for Data Science (excluding pc's)")


# ## TPU Usage

# In[52]:


tpu_country = df_survey_responses.groupby(['Q13', 'Q3'])
df_tpu_country = tpu_country.agg('count')
df_tpu_country.reset_index(inplace=True)
df_tpu_country=df_tpu_country[['Q13', 'Q3', 'Q2']]
df_tpu_country.columns=('tpu_usage', 'country', 'Count')
#sort by count
df_tpu_country = df_tpu_country.sort_values(by=['Count'], ascending=False)
df_tpu_country.head()
df_tpu_country['rank'] = df_tpu_country['tpu_usage']
tpu_sort_map = {"More than 25 times": 0, '6-25 times': 1, 
                  "2-5 times": 2,
                  "Once": 3, "Never": 4
                 }
df_tpu_country['rank'] = df_tpu_country['rank'].replace(tpu_sort_map)
df_tpu_country['tpu_usage_rank'] = df_tpu_country['rank'].astype(str) + ' - ' + df_tpu_country['tpu_usage']


# In[53]:


alt.themes.enable('latimes')
bars = alt.Chart(df_tpu_country).mark_bar(
        cornerRadiusTopLeft=3,
        cornerRadiusTopRight=3
    ).encode(
        y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
        x=alt.X('Count:Q', stack="normalize", title="Normalized Count"),
        color=alt.Color('tpu_usage_rank:N', legend=alt.Legend(orient="top"), title="TPU Usage")
    ).properties(width=400, height=500,title={
    "text": ["TPU Usage"],
    "subtitle": ["Broken Down proportionally by Country"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    })
bars


# ## Years using Machine Learning methods

# In[54]:


ml_country = df_survey_responses.groupby(['Q15', 'Q3'])
df_ml_country = ml_country.agg('count')
df_ml_country.reset_index(inplace=True)
df_ml_country=df_ml_country[['Q15', 'Q3', 'Q2']]
df_ml_country.columns=('ml_years', 'country', 'Count')
#sort by count
df_ml_country = df_ml_country.sort_values(by=['Count'], ascending=False)
df_ml_country.head()


# In[55]:


df_ml_country['ml_years'].unique()


# In[56]:


df_ml_country['rank'] = df_ml_country['ml_years']
ml_sort_map = {"I do not use machine learning methods": 0, 'Under 1 year': 1, 
                  "1-2 years": 2, "2-3 years": 3, "3-4 years": 4,
                "4-5 years":5, "5-10 years":6, "10-20 years":7,
                "20 or more years":8
                  
                 }
df_ml_country['rank'] = df_ml_country['rank'].replace(ml_sort_map)
df_ml_country['ml_years_rank'] = df_ml_country['rank'].astype(str) + ' - ' + df_ml_country['ml_years']
df_ml_country = df_ml_country.sort_values(by=['ml_years_rank'], ascending=False)
df_ml_country.head()


# In[57]:


alt.themes.enable('default')
alt.Chart(df_ml_country).mark_bar().encode(
        y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
        x=alt.X('Count:Q', title="Count"),
        color=alt.Color('ml_years_rank:N', legend=alt.Legend(orient="top"), title="Years using ML methods")
    ).properties(width=400, height=500,title={
    "text": ["Years using ML methods"],
    "subtitle": ["Broken Down by Country"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    }
    )


# In[58]:


bars_ml = alt.Chart(df_ml_country).mark_bar(
        cornerRadiusTopLeft=3,
        cornerRadiusTopRight=3
    ).encode(
        y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
        x=alt.X('Count:Q', stack="normalize", title="Normalized Count"),
        color=alt.Color('ml_years_rank:N', legend=alt.Legend(orient="top"), title="Years using ML methods")
    ).properties(width=500, height=500,title={
    "text": ["Years using ML methods"],
    "subtitle": ["Broken Down proportionally by Country"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    })
bars_ml


# In[59]:


# Add the country code for a new choropleth map
df_ml_country['country_code'] = df_ml_country['country']
df_ml_country.replace({"country_code": country_codes}, inplace=True)
#merge to get total respondents per country
df_ml_country = df_ml_country.merge(df_country, left_on='country', right_on='Country', suffixes=(None, '_total'))


# In[60]:


df_ml_country['ML_experience_freq_pct'] = 100 * df_ml_country['Count'] / df_ml_country['Count_total']
df_ml_country


# In[61]:


df_ml_country_5_plus = df_ml_country[df_ml_country['ml_years_rank'].isin(['6 - 5-10 years', '7 - 10-20 years','8 - 20 or more years'])]
df_ml_country_5_plus


# In[63]:


fig = px.choropleth(data_frame=df_ml_5_plus,
                    locations='country_code',
                    color='ML_experience_freq_pct',
                    color_continuous_scale="agsunset",
                    range_color=(0, 25),
                    hover_name='country_code', # column to add to hover information
                    title='% Respondents with 5+ years experience of ML Methods')
fig.show()


# ## Primary Analysis Toolset

# In[64]:


primary_analysis = df_survey_responses.groupby(['Q38', 'Q3'])
df_primary_analysis = primary_analysis.agg('count')
df_primary_analysis.reset_index(inplace=True)
df_primary_analysis=df_primary_analysis[['Q38', 'Q3', 'Q2']]
df_primary_analysis.columns=('primary_analysis', 'country', 'Count')
df_primary_analysis['logCount'] = np.log(df_primary_analysis['Count'])
#sort by count
df_primary_analysis = df_primary_analysis.sort_values(by=['Count'], ascending=False)
df_primary_analysis


# In[65]:


df_primary_analysis['primary_analysis'].unique()


# In[66]:


df_primary_analysis['primary_analysis_abbr'] = df_primary_analysis['primary_analysis']
primary_analysis_sort_map = {'Local development environments (RStudio, JupyterLab, etc.)': 'Local development environments',
                             'Basic statistical software (Microsoft Excel, Google Sheets, etc.)': 'Basic statistical software',
                            'Business intelligence software (Salesforce, Tableau, Spotfire, etc.)': 'Business intelligence software',
                            'Cloud-based data software & APIs (AWS, GCP, Azure, etc.)': 'Cloud-based data software & APIs',
                            'Advanced statistical software (SPSS, SAS, etc.)': 'Advanced statistical software',
                 }
df_primary_analysis['primary_analysis_abbr'] = df_primary_analysis['primary_analysis_abbr'].replace(primary_analysis_sort_map)
df_primary_analysis


# In[67]:


def analysis_altair(df, colorfeat, colortitle):
    chrt = alt.Chart(df).mark_rect().encode(
        x=alt.X('country:N'),
        y=alt.Y('primary_analysis_abbr:N', title="Primary Analysis Toolset"),
        color=alt.Color(colorfeat, legend=alt.Legend(orient="bottom"), title=colortitle)
    ).properties(width=600, height=160,
                 background='#f5f5f5',title={
    "text": ["Primary Analysis Toolset Used by Respondents"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    })
    
    return chrt


# In[68]:


analysis_log_chrt = analysis_altair(df_primary_analysis, 'logCount:Q', 'Log Count')
analysis_log_chrt


# In[69]:


df_primary_analysis = df_primary_analysis.merge(df_country, left_on='country', right_on='Country',
          suffixes=(None, '_total'))
df_primary_analysis['primary_analysis_pct'] = round(100 * df_primary_analysis['Count'] / df_primary_analysis['Count_total'],1)
df_primary_analysis = df_primary_analysis.sort_values(by=['primary_analysis_pct'], ascending=False)
df_primary_analysis


# In[70]:


analysis_pct_chrt = analysis_altair(df_primary_analysis, 'primary_analysis_pct:Q', '% by Country')
analysis_pct_chrt


# In[71]:


df_primary_analysis.loc[df_primary_analysis['Country']=='Japan']


# In[72]:


# Add the country code for a new choropleth map
df_primary_analysis['country_code'] = df_primary_analysis['country']
df_primary_analysis.replace({"country_code": country_codes}, inplace=True)


# In[73]:


df_primary_analysis_local = df_primary_analysis.loc[df_primary_analysis['primary_analysis_abbr']=='Local development environments']


# In[74]:


fig = px.choropleth(data_frame=df_primary_analysis_local,
                    locations='country_code',
                    color='primary_analysis_pct',
                    color_continuous_scale="agsunset",
                    range_color=(0, 50),
                    hover_name='Country', # column to add to hover information
                    title='% Respondents primarily using Local Development Environment')
fig.show()


# ## Company Size

# In[75]:


company_size = df_survey_responses.groupby(['Q20', 'Q3'])
df_company_size = company_size.agg('count')
df_company_size.reset_index(inplace=True)
df_company_size=df_company_size[['Q20', 'Q3', 'Q2']]
df_company_size.columns=('company_size', 'country', 'Count')
df_company_size['rank'] = df_company_size['company_size']
company_size_sort_map = {"10,000 or more employees": 4, '1000-9,999 employees': 3, 
                  "250-999 employees": 2,
                  "50-249 employees": 1, "0-49 employees": 0
                 }
df_company_size['rank'] = df_company_size['rank'].replace(company_size_sort_map)
df_company_size['company_size_rank'] = df_company_size['rank'].astype(str) + ' - ' + df_company_size['company_size']
df_company_size


# In[76]:


bars_company_size = alt.Chart(df_company_size).mark_bar(
        cornerRadiusTopLeft=3,
        cornerRadiusTopRight=3
    ).encode(
        y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
        x=alt.X('Count:Q', stack="normalize", title="Normalized Count"),
        color=alt.Color('company_size_rank:N', legend=alt.Legend(orient="bottom"), title="Company Size")
    ).properties(width=500, height=500,title={
    "text": ["Company Size of Respondent's Employer"],
    "subtitle": ["Broken Down proportionally by Country"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    })

bars_company_size


# ## How many people are responsible for Data Science workloads at the place of business?

# In[77]:


data_scientists = df_survey_responses.groupby(['Q21', 'Q3'])
df_data_scientists = data_scientists.agg('count')
df_data_scientists.reset_index(inplace=True)
df_data_scientists=df_data_scientists[['Q21', 'Q3', 'Q2']]
df_data_scientists.columns=('data_scientists', 'country', 'Count')

df_data_scientists['rank'] = df_data_scientists['data_scientists']
data_scientists_sort_map = {"20+": 6, "15-19": 5, "10-14": 4, '5-9': 3, 
                  "3-4": 2,
                  "1-2": 1, "0": 0
                 }
df_data_scientists['rank'] = df_data_scientists['rank'].replace(data_scientists_sort_map)
df_data_scientists['data_scientists_rank'] = df_data_scientists['rank'].astype(str) + ' - ' + df_data_scientists['data_scientists']
data_scientists = df_survey_responses.groupby(['Q21', 'Q3'])
df_data_scientists = data_scientists.agg('count')
df_data_scientists.reset_index(inplace=True)
df_data_scientists=df_data_scientists[['Q21', 'Q3', 'Q2']]
df_data_scientists.columns=('data_scientists', 'country', 'Count')

df_data_scientists['rank'] = df_data_scientists['data_scientists']
data_scientists_sort_map = {"20+": 6, "15-19": 5, "10-14": 4, '5-9': 3, 
                  "3-4": 2,
                  "1-2": 1, "0": 0
                 }
df_data_scientists['rank'] = df_data_scientists['rank'].replace(data_scientists_sort_map)
df_data_scientists['data_scientists_rank'] = df_data_scientists['rank'].astype(str) + ' - ' + df_data_scientists['data_scientists']
df_data_scientists


# In[78]:


bubble_plot_company_size = alt.Chart(df_data_scientists).mark_circle(
        cornerRadiusTopLeft=3,
        cornerRadiusTopRight=3
    ).encode(
        y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
        x=alt.X('Count:Q', title="Count"),
        color=alt.Color('data_scientists_rank:N', legend=alt.Legend(orient="bottom"), title=""),
        size=alt.Size('Count:Q'),    
    ).properties(width=500, height=500,title={
    "text": ["Number of people responsible for Data Science workloads at place of Business"],
    "subtitle": ["Broken Down proportionally by Country"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    })
bubble_plot_company_size


# ## Machine Learning

# In[79]:


ml = df_survey_responses.groupby(['Q22', 'Q3'])
df_ml = ml.agg('count')
df_ml.reset_index(inplace=True)
df_ml=df_ml[['Q22', 'Q3', 'Q2']]
df_ml.columns=('ml', 'country', 'Count')
df_ml['ml'].unique()


# In[80]:


df_ml_prod = df_ml.loc[df_ml['ml'].isin(['We have well established ML methods (i.e., models in production for more than 2 years)',
       'We recently started using ML methods (i.e., models in production for less than 2 years)'])]
df_ml_prod = df_ml_prod.drop(columns=['ml'])
#get prod totals
ml_prod = df_ml_prod.groupby(['country'])
df_ml_prod = ml_prod.agg('sum').reset_index()
#get overall totals
ml_total = df_ml.groupby(['country'])
df_ml_total = ml_total.agg('sum').reset_index()
df_ml_prod = df_ml_prod.merge(df_ml_total[['country', 'Count']], left_on='country', right_on='country',
          suffixes=('_prod', '_total'))
df_ml_prod['country_code'] = df_ml_prod['country']
df_ml_prod.replace({"country_code": country_codes}, inplace=True)
df_ml_prod['ml_in_prod_pct'] = round(100 * df_ml_prod['Count_prod'] / df_ml_prod['Count_total'],1)
df_ml_prod
df_ml_prod_sorted = df_ml_prod.sort_values(by=['ml_in_prod_pct'], ascending=False)
df_ml_prod_sorted


# In[81]:


def ml_prod(df, subtitle, sort=None):
    ml_prod_chart = alt.Chart(df_ml_prod).mark_bar(
            cornerRadiusTopLeft=3,
            cornerRadiusTopRight=3
        ).encode(
            y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0), sort=sort),
            x=alt.X('ml_in_prod_pct:Q', title="%"),
        ).properties(width=200, height=500, title={
        "text": ["ML models in Production"],
        "subtitle": subtitle,        
        "fontSize":16, "fontWeight":600, "subtitleFontSize":12
        })
    return ml_prod_chart
bar_years_ml = ml_prod(df_ml_prod, "% whose place of Business Uses")
bar_years_ml_sorted = ml_prod(df_ml_prod_sorted, "% whose place of Business Uses", "-x")
bar_years_ml | bar_years_ml_sorted


# ## Rates of Pay

# In[83]:


pay = df_survey_responses.groupby(['Q24', 'Q3'])
df_pay = pay.agg('count')
df_pay.reset_index(inplace=True)
df_pay=df_pay[['Q24', 'Q3', 'Q2']]
df_pay.columns=('pay', 'country', 'Count')

df_pay['pay'].unique()


# In[84]:


df_pay['rank'] = df_pay['pay']
pay_sort_map = {"> $500,000": '24', "300,000-500,000": '23', "250,000-299,999": '22', '200,000-249,999': '21',
                "150,000-199,999": '20', "125,000-149,999": '19', "100,000-124,999": '18', '90,000-99,999': '17',
                "80,000-89,999": '16', "70,000-79,999": '15', "60,000-69,999": '14', '50,000-59,999': '13',
                "40,000-49,999": '12', "30,000-39,999": '11', "25,000-29,999": '10', '20,000-24,999': '09',
                "15,000-19,999": '08', "10,000-14,999": '07', "7,500-9,999": '06', '5,000-7,499': '05',
                "4,000-4,999": '04', "3,000-3,999": '03', "2,000-2,999": '02', '1,000-1,999': '01', 
                '$0-999': '00'
                 }
df_pay['rank'] = df_pay['rank'].replace(pay_sort_map)
df_pay['pay_rank'] = df_pay['rank'].astype(str) + ' - ' + df_pay['pay']

df_pay

df_pay = df_pay.merge(df_country[['Country', 'Count']], left_on='country', right_on='Country',
          suffixes=('_pay_category', '_total'))
df_pay['pay_pct'] = round(100 * df_pay['Count_pay_category'] / df_pay['Count_total'],1)
df_pay


# In[85]:


bubble_plot_pay = alt.Chart(df_pay).mark_circle(
        cornerRadiusTopLeft=3,
        cornerRadiusTopRight=3
    ).encode(
        y=alt.Y('country:N', axis=alt.Axis(grid=False, titleAngle=0)),
        x=alt.X('pay_rank:O', title="Pay", axis=alt.Axis(grid=False, labelAngle=270)),
        color=alt.Color('pay_rank:O', legend=None, title=""),
        size=alt.Size('pay_pct:Q'),    
    ).properties(width=570, height=500,title={
    "text": ["Salaries"],
    "subtitle": ["Broken Down proportionally by Country"],
    "fontSize":18, "fontWeight":600, "subtitleFontSize":14
    })
bubble_plot_pay


# ## Conculsion:

# Data Science is flourishing in 2020. We can see respondents to the Kaggle survey from over 50 different countries. By working through the data in the survey breaking down the information by country we have found many interesting insights. For example respondents from India have tended to be younger than those from the US. Respondents from Iran and Switzerland have been more likely to have a PhD. Respondents from Sweden were more likely to have 20+ years of coding experience. Deep Learning platforms are more popular than cloud-based platforms with respondents in Belgium and Viet Nam. TPU usage was highest amongst respondents in Viet Nam. Respondents from Switzerland are most likely to have 5 or more years of experience using machine learning methods. In India and the USA, respondents were most likely to work for companies with 20 or more Data Scientists but in Brazil, Canada and China respondents were most likely to work for companies where there were only 1-2 people working on Data Science. Based on observations of respondents, ML models were most likely to reach production in Israel followed by Belarus and the UK.
# 
# However, as per the caveats previously stated, we must recognise that we are looking at the real-world data available in the survey. This sample may suffer from selection bias and sampling error. The results noted here are based on this data and may not all extrapolate to be fully representative of all countries for all of the questions we have looked at.